# svelte-matomo component

This component loads the matomo script and exposes an asynchronous matomo api.

## Usage in svelte

```html
<script>
  import { onMount } from 'svelte'
  import Matomo, { matomo } from '@dexlib/svelte-matomo'

  const url = '<matomo-url>'
  const siteId = 0

  onMount(() => {
    matomo.trackPageView()
  })
</script>

<Matomo {url} {siteId} />
```

## Usage in sapper
Is very similar, but set it up in your most upper `_layout.svelte` page component. You can use the page store for reacting to page changes:

* _layout.html *
```html
<script>
  import { stores } from '@sapper/app'
  import Matomo, { matomo } from '@dexlib/svelte-matomo'

  const { page } = stores()

  const url = '<matomo-url>'
  const siteId = 0
  
  $: if ($page) matomo.trackPageView()

  onMount(() => {
    matomo.trackPageView()
  })
</script>

<Matomo {url} {siteId} />
```

Use the [matomo api](https://developer.matomo.org/api-reference/tracking-javascript) for further reference.
